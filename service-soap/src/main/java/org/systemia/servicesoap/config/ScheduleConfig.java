package org.systemia.servicesoap.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableScheduling
public class ScheduleConfig {
	
	@Scheduled(cron = "*/3 * * * * *")
	void execuTask() {
		log.info("Execute schedule");
	}
}

package org.systemia.servicesoap.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableCaching
@RequiredArgsConstructor
public class CacheConfig {
	
	private final CacheManager cacheManager;
	
//	@Scheduled(cron = "*/3 * * * * *")
	@CacheEvict(value = "countries", allEntries = true)
	public void cleaningCache() {
		log.info("[CACHE] cleaning cache all entrie");
	}
	
	@Scheduled(cron = "*/5 * * * * *")
	public void showEntriesFromCachin() {
		log.info("[ENTRIES-CACHE] all entries {}", cacheManager.getCacheNames());
	}
}
